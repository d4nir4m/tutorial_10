package com.example;



import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import org.junit.Assert;

import com.example.model.StudentModel;
import com.example.service.StudentService;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
public class StudentServiceTests {
	@Autowired
	StudentService service;

	@Test
	public void testSelectAllStudents() {
		List<StudentModel> students = service.selectAllStudents();
		Assert.assertNotNull("Gagal = student menghasilkan null", students);
		Assert.assertEquals("Gagal - size students tidak sesuai", 5, students.size());

	}
	
	@Test
	public void testSelectStudent() {
		StudentModel student = service.selectStudent ("123");
		Assert.assertNotNull("Gagal = student menghasilkan null", student);
		Assert.assertEquals("Gagal - GPA student tidak sesuai", 3.5,student.getGpa(), 0.0);

	}
	
	@Test
	public void testCreateStudent() {
		StudentModel student = new StudentModel("126", "Budi", 3.44, null);
		// Cek apakah student sudah ada
		Assert.assertNull("Mahasiswa sudah ada", service.selectStudent(student.getNpm()));
		// Masukkan ke service
		service.addStudent(student);
		// Cek apakah student berhasil dimasukkan
		Assert.assertNotNull("Mahasiswa gagal dimasukkan", service.selectStudent(student.getNpm()));
	}
	
	@Test
	public void testUpdateStudent() {
		StudentModel student = service.selectStudent ("123");
		// Cek apakah student ada
		//Assert.assertNull("Mahasiswa tidak ada", service.selectStudent(student.getNpm()));
		Assert.assertNotNull("Gagal = student menghasilkan null", student);
		// Masukkan ke service
		StudentModel studentUpdate = new StudentModel (student.getNpm(),student.getName(), 3.6, null);
		service.updateStudent(studentUpdate);
		// Cek apakah student berhasil diupdate
		Assert.assertNotNull("Mahasiswa gagal diupdate", service.selectStudent(studentUpdate.getNpm()));
		// Cek Kembali Student tsb
		StudentModel studentCek = service.selectStudent (student.getNpm());
		Assert.assertNotNull("Gagal = student menghasilkan null", studentCek);
		Assert.assertEquals("Gagal - GPA student tidak sesuai", 3.6,studentCek.getGpa(), 0.0);
	}
	
	@Test
	public void testDeleteStudent() {
		StudentModel student = service.selectStudent ("123");
		// Cek apakah student ada
		//Assert.assertNull("Mahasiswa tidak ada", service.selectStudent(student.getNpm()));
		Assert.assertNotNull("Gagal = student menghasilkan null", student);
		// Masukkan ke service
		service.deleteStudent(student.getNpm());
		// Cek apakah student berhasil didelete
		Assert.assertNotNull("Mahasiswa gagal didelete", student.getNpm());
		// Cek Kembali Student tsb
		StudentModel studentCek = service.selectStudent (student.getNpm());
		Assert.assertNotNull("Gagal = student menghasilkan null", studentCek);
	}
}
